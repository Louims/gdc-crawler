import json
import urllib.request
import re


from flask import Flask
from flask import render_template

app = Flask(__name__)


lat = "45.7578137"
lon = "4.8320114"
radius = "100"
data_gdc = urllib.request.urlopen(
    "https://gensdeconfiance.com/api/v2/classified?category=realestate__sale&orderColumn=displayDate&orderDirection"
    "=DESC&type=offering&rootLocales[]=en&rootLocales[]=fr&"
    "lat=" + lat +
    "&lon=" + lon +
    "&radius=" + radius +
    "&minPrice=75000&maxPrice=390000&minSquareMeters=70&hidden=true&page=1"
    "&itemsPerPage=50"
    "").read()
raw_output_gdc = json.loads(data_gdc)
gdc_without_pro = [x for x in raw_output_gdc if not x['pro']]
keys_i_want = ('id', 'title', 'price', 'description', 'city', 'zip')


@app.route('/')
def hello():
    all_offers = []
    for gdc_offer_real_estate in gdc_without_pro:
        test = {
            'id': gdc_offer_real_estate['id'],
            'title': gdc_offer_real_estate['title'],
            'price': gdc_offer_real_estate['price'],
            'description': gdc_offer_real_estate['description'],
            'city': gdc_offer_real_estate['city'],
            'zip': gdc_offer_real_estate['zip'],
            'accurateLocation': gdc_offer_real_estate['accurateLocation']
        }
        match = re.search('([0-9]+) *m', gdc_offer_real_estate['title'])
        if match:
            test['square_meter'] = int(match.groups()[0])
            test['price_by_square_meter'] = gdc_offer_real_estate['price'] / test['square_meter']
        else:
            match_description = re.search('([0-9]+) *m', gdc_offer_real_estate['description'])
            if match_description:
                test['square_meter'] = int(match_description.groups()[0])
                test['price_by_square_meter'] = gdc_offer_real_estate['price'] / test['square_meter']
        all_offers.append(test)

    return render_template('offers.html',
                           offers=all_offers,
                           keys=gdc_without_pro[0].keys()
                           )
