#!/usr/bin/env python
import http.server

PORT = 8889
server_address = ("", PORT)

server = http.server.HTTPServer
#handler = http.server.CGIHTTPRequestHandler
handler = http.server.SimpleHTTPRequestHandler
handler.cgi_directories = ["/"]
print("Serveur actif sur le port :", PORT)

httpd = server(server_address, handler)
httpd.serve_forever()
