from collections import defaultdict
import json
import urllib.request

lat = "45.7578137"
lon = "4.8320114"

data_dvf = json.loads(urllib.request.urlopen(
    "http://api.cquest.org/dvf?"
    "lat=" + lat +
    "&lon=" + lon +
    "&dist=500" +  # default 500m radius
    "").read())
# gdc_without_pro = [x for x in data_dvf.features if x['type_local'] in ("Appartement")]
# var = {k: v for k, v in data_dvf['features'] if v['type_local'] == 'Appartement'}
# filtered = filter(lambda score: score['properties']['type_local'] == 'Appartement', data_dvf['features'])
average_square_meter_price = list()
for feature in data_dvf['features']:
    if {'surface_relle_bati', 'valeur_fonciere'}.issubset(feature['properties']) and feature['properties']['surface_relle_bati'] != 0:
        price_by_square_meter = feature['properties']['valeur_fonciere'] / feature['properties']['surface_relle_bati']
        if 300 < price_by_square_meter < 20000:
            average_square_meter_price.append(price_by_square_meter)

min_average = min(average_square_meter_price)
max_average = max(average_square_meter_price)
distance = max_average - min_average
step = distance / 20

dvfAverageLabels = list()

print(min_average)
print(max_average)

dvfAverageLabelsSteps = list(range(0, 21))
for dvfAverageLabelsStep in dvfAverageLabelsSteps:
    dvfAverageLabels.append(dvfAverageLabelsStep * step + min_average)

dvfAverageSum = defaultdict(int)
for square_meter_price in average_square_meter_price:
    key = int((square_meter_price - min_average) / step)
    if key != 0:
        dvfAverageSum[key] += 1

print(dvfAverageLabels)
print(list(dvfAverageSum))
